# Copyright 2018-2018 DevCraft, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feature: Surface Properties

Scenario: Computer pixel type
    Given rendering surface is Computer Screen
    When create pixel (100%, 100%, 100%, 50%)
    Then pixel = rgba32(255, 255, 255, 127)

Scenario: Computer layout type
    Given rendering surface is Computer Screen
    When select layout
    Then layout = Large Landscape

Scenario: Tablet pixel type
    Given rendering surface is Tablet Screen
    When create pixel (100%, 100%, 100%, 50%)
    Then pixel = rgba24(63, 63, 63, 31)

Scenario: Tablet layout type
    Given rendering surface is Tablet Screen
    When select layout
    Then layout = Medium Landscape

Scenario: Phone pixel type
    Given rendering surface is Phone Screen
    When create pixel (100%, 100%, 100%, 50%)
    Then pixel = rgba24(63, 63, 63, 31)

Scenario: Phone layout type
    Given rendering surface is Phone Screen
    When select layout
    Then layout = Tiny Portrait