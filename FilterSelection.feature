# Copyright 2018-2018 DevCraft, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feature: Filter-selection

Scenario: All options
    Given Green-Screen filter flag is on
    And Green-Screen color is Pure Red with tolerance of 2%
    And Ghost filter flag is on
    And Ghost filter factor is 25%
    And Deaden filter flag is on
    And Deaden filter color is Pure Blue
    And Alpha Floor filter flag is on
    And Alpha Floor filter threshold is 5%
    And Alpha Ceiling filter flag is on
    And Alpha Cieling filter flag is 60%
    And Texture Projection filter uses Sprite S
    When pixel pipeline is created from settings
    Then pixel pipeline has the following phases:
        | Type          | Description           |
        | Projection    | For Sprite S          |
        | Green-Screen  | Within 2% of Pure Red |
        | Deaden        | Subtract out Pure Blue|
        | Ghost         | Scale Alpha by 75%    |
        | Alpha Floor   | Minimum Alpha 5%      |
        | Alpha Ceiling | Maximum Alpha 60%     |

Scenario Outline: Filter-Selection
    Given Green-Screen filter flag is <GS>
    And Ghost filter flag is <G>
    And Deaden filter flag is <D>
    And Alpha Floor filter flag is <AF>
    And Alpha Ceiling filter flag is <AC>
    When pixel pipeline is created from settings
    Then pixel pipline has these layers: <Layers>
Scenarios:
    | GS  | G   | D   | AF  | AC  | Layers                                                              |
    | on  | on  | on  | on  | on  | Projection, Green-Screen, Deaden, Ghost, Alpha Floor, Alpha Ceiling |
    | off | off | off | off | off | Projection                                                          |
    | on  | off | off | off | off | Projection, Green-Screen                                            |
    | off | on  | off | off | off | Projection, Ghost                                                   |
    | off | off | on  | off | off | Projection, Deaden                                                  |
    | off | off | off | on  | off | Projection, Alpha Floor                                             |
    | off | off | off | off | on  | Projection, Alpha Ceiling                                           |